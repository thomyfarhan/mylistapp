package id.co.iconpln.mylistapp

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val listHero: ListView
        get() = lv_list_hero

    private var list: ArrayList<Hero> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        
//        loadListArrayAdapter()
        loadListBaseAdapter(this)
        setListItemClickListener(listHero)
    }

    private fun setListItemClickListener(listView: ListView) {
        listView.setOnItemClickListener { _, _, index, _ ->
            Toast.makeText(this, list[index].name, Toast.LENGTH_SHORT).show()
            showDetailHero(list[index])
        }
    }

    private fun showDetailHero(hero: Hero) {
        val detailHeroIntent = Intent(this, DetailHeroActivity::class.java)
        detailHeroIntent.putExtra(DetailHeroActivity.EXTRA_HERO, hero)
        startActivity(detailHeroIntent)
    }

    private fun loadListBaseAdapter(context: Context) {
        list.addAll(HeroesData.listDataHero)

        val baseAdapter = ListViewHeroAdapter(context, list)
        listHero.adapter = baseAdapter
    }

    private fun loadListArrayAdapter() {
        val adapter = ArrayAdapter<String>(this,
            android.R.layout.simple_list_item_1, getDataHero())

        listHero.adapter = adapter
    }

    private fun getDataHero(): Array<String> {
        val hero = arrayOf(
            "Cut Nyak Dien", "Ki Hajar Dewantara", "Moh. Yamin", "Patimura", "R. A. Kartini", "Sukarno"
        )
        return hero
    }
}
