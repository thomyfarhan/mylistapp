package id.co.iconpln.mylistapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_detail_hero.*

class DetailHeroActivity : AppCompatActivity() {

    private lateinit var hero: Hero

    companion object {
        const val EXTRA_NAME = "name"
        const val EXTRA_DESC = "desc"
        const val EXTRA_IMAGE_URL = "image_url"
        const val EXTRA_HERO = "hero"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_hero)

        setUpActionBar()
        displayHeroDetail()
    }

    private fun setUpActionBar() {
        supportActionBar?.title = getString(R.string.detail_hero_text)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun displayHeroDetail() {
        hero = intent.getParcelableExtra(EXTRA_HERO) ?: Hero()
        tvDetailHeroName.text = hero.name
        tvDetailHeroDesc.text = hero.desc
        Glide.with(this)
            .load(hero.photo)
            .apply(
                RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_visibility_black_24dp)
                    .error(R.drawable.ic_error_black_24dp)
            )
            .into(ivDetailHeroImage)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
